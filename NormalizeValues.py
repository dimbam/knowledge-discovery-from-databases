from sklearn import preprocessing
import pandas as pd
import csv
import numpy as np
import os

#ορίστε το path του αρχείου για κανονικοποίηση μεσα στην παρακάτω παρένθεση
inputfile = 'C:/Users/JimBam/Downloads/iris.csv'
df = pd.read_csv(inputfile)
dflast = pd.read_csv(inputfile)

print(df.columns[0:len(df.columns)])
dflast = dflast['class']
df.drop(columns=df.columns[-1:], axis=1, inplace=True)

x = df.values 
min_max_scaler = preprocessing.MinMaxScaler()
x_scaled = min_max_scaler.fit_transform(x)
newdf = pd.DataFrame(x_scaled)


#ορίστε το path για το παραγώμενο αρχείο 
f = open("normalized.csv", "w")
#προσθήκη labels στο αρχείο
for i in range(len(df.columns)):
    f.write(str(df.columns[i]))
    f.write(',')
f.write('class')    
f.write('\n')
f.close()

df_all_rows = pd.concat([newdf, dflast], axis = 1)
#print(df_all_rows)

#ορίστε το path για το παραγώμενο αρχείο
#εγγραφή του νέου dataframe στο αρχείο

#df_all_rows.to_csv('normalized.csv', index=False)
df_all_rows.to_csv('normalized.csv', header=None, index=None, sep=',', mode='a')
print("wrote file")




