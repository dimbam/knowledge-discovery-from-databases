from struct import pack, unpack


def VariableByteCodeEncoder(num):
    if num >= 1:
        binary = bin(num)[2:]
        # print(len(binary))

        if num < 127:
            if len(binary) < 7:
                binary = '1' + '0' * (7 - len(binary)) + binary
            else:
                binary = '1' + binary

            print(str(num) + " is " + str(binary) + " with variable byte code ")

        else:
            finalBinary = ''
            last7Bits = int('1' + binary[len(binary) - 7:])
            higherBits = ''
            binary = binary[::-1]

            for i in range(7, len(binary), 7):
                # add 0 to the 7 bits
                higherBits = binary[i:i + 7]
                # reverse the higher bits
                higherBits = higherBits[::-1]

                if len(higherBits) < 8:
                    higherBits = '0' * (8 - len(higherBits)) + higherBits

                finalBinary = higherBits + finalBinary

            finalBinary += str(last7Bits)

            # print the final binary
            print(str(num) + " is " + str(finalBinary) +
                  " with variable byte code ")
    else:
        print("Give another number")


def binaryToDecimal(binary):

    decimal, i = 0, 0
    while (binary != 0):
        dec = binary % 10
        decimal = decimal + dec * pow(2, i)
        binary = binary//10
        i += 1
    return decimal


def VariableByteCodeDecoder(binary):

    index = 0
    binStr = str(binary)
    count = 0
    t = '01'
    for char in binStr:
        if char not in t:
            count = 1
            print("Please give a binary number: ")
            break
        else:
            pass
    if count == 0:
        if len(binStr) < 9:
            bin = binStr[1:: 1]
            print("Variable byte code " + str(binary) + " is: " +
                  str(binaryToDecimal(int(bin))) + " to decimal")
        else:
            for i in range(len(binStr)):
                if binStr[i] != '0':
                    index = i
                    break
            newb = binStr[index::1]
            newb2 = newb[0:(len(newb)-8)]
            newb3 = newb[(len(newb)-7):]
            finalbin = newb2 + newb3
            print("Variable byte code " + str(binary) + " is: " +
                  str(binaryToDecimal(int(finalbin))) + " to decimal")


# Examples
# Τα ορίσματα δεν θα πρέπει να έχουν κενά ανάμεσα στο 8bit blocks

VariableByteCodeDecoder('0000000111001001')
VariableByteCodeDecoder('10010001')
VariableByteCodeDecoder('0000011010001001')
VariableByteCodeDecoder('11010000')
VariableByteCodeEncoder(201)
VariableByteCodeEncoder(17)
VariableByteCodeEncoder(80)
